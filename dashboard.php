<?php
/**
 * Assets - Dashboard
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("assets"));
 // build dashboard object
 $dashboard=new strDashboard();
 $dashboard->addTile("?mod=".MODULE."&scr=assets_list",api_text("dashboard-assets-list"),api_text("dashboard-assets-list-description"),(api_checkAuthorization("assets-usage")),"1x1","fa-book"); /** @todo permessi */
 $dashboard->addTile("?mod=".MODULE."&scr=employees_list",api_text("dashboard-employees-list"),api_text("dashboard-employees-list-description"),(api_checkAuthorization("assets-usage")),"1x1","fa-group"); /** @todo permessi */
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dashboard->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
?>