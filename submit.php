<?php
/**
 * Assets - Submit
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // debug
 api_dump($_REQUEST,"_REQUEST");
 // switch action
 switch(ACTION){
  // assets
  case "asset_store":asset("store");break;
  //case "asset_status":asset("status");break;
  case "asset_delete":asset("delete");break;
  case "asset_undelete":asset("undelete");break;
  case "asset_remove":asset("remove");break;
  // employees
  case "employee_asset_add":employee_asset_add();break;
  // default
  default:
   api_alerts_add(api_text("alert_submitFunctionNotFound",[MODULE,SCRIPT,ACTION]),"danger");
   api_redirect("?mod=".MODULE);
 }

 /**
  * Asset
  *
  * @param string $action Object action
  */
 function asset($action){
  // check authorizations
  api_checkAuthorization("assets-manage","dashboard");
  // get object
  $asset_obj=new cAssetsAsset($_REQUEST['idAsset']);
  api_dump($asset_obj,"asset object");
  // check object
  if($action!="store" && !$asset_obj->id){api_alerts_add(api_text("cAssetsAsset-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=assets_list");}
  // execution
  try{
   switch($action){
    case "store":
     // encode properties into json
     $_REQUEST['properties_json']=(count($_REQUEST['properties'])?json_encode($_REQUEST['properties']):null);
     $asset_obj->store($_REQUEST);
     api_alerts_add(api_text("cAssetsAsset-alert-stored"),"success");
     break;
    /*case "status":
     $asset_obj->status($_REQUEST['status'],["note"=>$_REQUEST['note']]);
     api_alerts_add(api_text("cAssetsAsset-alert-status"),"success");
     break;*/
    case "delete":
     $asset_obj->delete();
     api_alerts_add(api_text("cAssetsAsset-alert-deleted"),"warning");
     break;
    case "undelete":
     $asset_obj->undelete();
     api_alerts_add(api_text("cAssetsAsset-alert-undeleted"),"warning");
     break;
    case "remove":
     $asset_obj->remove();
     api_alerts_add(api_text("cAssetsAsset-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Asset action \"".$action."\" was not defined..");
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"assets_list","idAsset"=>$asset_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"assets_list","idAsset"=>$asset_obj->id]),"cAssetsAsset-alert-error");
  }
 }



 /**
  * Employee Asset Add
  */
 function employee_asset_add(){
  // check authorizations
  api_checkAuthorization("assets-manage","dashboard");
  // get objects
  $employee_obj=new cEmployeesEmployee($_REQUEST['idEmployee']);
  $asset_obj=new cAssetsAsset($_REQUEST['idAsset']);
  api_dump($employee_obj,"employee object");
  api_dump($asset_obj,"asset object");
  // check object
  if(!$employee_obj->id){api_alerts_add(api_text("cEmployeesEmployee-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=employees_list");}
  if(!$asset_obj->id){api_alerts_add(api_text("cAssetsAsset-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=employees_list");}
  if($asset_obj->status!="available"){api_alerts_add(api_text("cAssetsAsset-alert-error"),"danger");api_redirect("?mod=".MODULE."&scr=employees_list");}
  // execution
  try{
   $asset_obj->store(array("fkEmployee"=>$employee_obj->id,"status"=>"assigned"));
   api_alerts_add(api_text("cAssetsAsset-alert-stored"),"success");
   // redirect
   api_redirect(api_return_url(["scr"=>"employees_list","idEmployee"=>$employee_obj->id,"idAsset"=>$asset_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"employees_list","idEmployee"=>$employee_obj->id]),"cEmployeesEmployee-alert-error");
  }
 }

?>