<?php
/**
 * Assets Functions
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // include classes
 require_once(DIR."modules/assets/classes/cAssetsAsset.class.php");
?>