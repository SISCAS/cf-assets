<?php
/**
 * Assets - Employees View (Assets)
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // definitions
 $assets_array=array();

 // build table
 $assets_table=new strTable(api_text("assets_view-assets-tr-unvalued"));
 $assets_table->addHeader("&nbsp;",null,16);
 $assets_table->addHeader(api_text("assets_list-th-barcode"),"nowrap");
 $assets_table->addHeader(api_text("assets_list-th-typology"),"nowrap");
 $assets_table->addHeader(api_text("assets_list-th-location"),"nowrap");
 $assets_table->addHeader(api_text("assets_list-th-name"),null,"100%");
 $assets_table->addHeader("&nbsp;",null,16);
 // build query object
 $query=new cQuery("assets__assets","fkEmployee='".$employee_obj->id."'");
 $query->addQueryOrderField("barcode");
 // cycle all results
 foreach($query->getRecords() as $result_f){$assets_array[$result_f->id]=new cAssetsAsset($result_f);}
 // cycle all assets
 foreach($assets_array as $asset_fobj){
  // build operation button
  //$ob=new strOperationsButton();
  //$ob->addElement(api_url(["scr"=>"submit","act"=>"asset_delete","idAsset"=>$asset_fobj->id,"return"=>["scr"=>"activities_view","tab"=>"assets","idActivity"=>$activity_obj->id]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("projects-projects_manage")),api_text("activities_view-assets-td-delete-confirm"));
  // make table row class                 /** @todo fare i return_scr */
  $tr_class_array=array();
  if($asset_fobj->id==$_REQUEST['idAsset']){$tr_class_array[]="info";}
  if($asset_fobj->deleted){$tr_class_array[]="deleted";}
  // make project row
  $assets_table->addRow(implode(" ",$tr_class_array));
  $assets_table->addRowFieldAction(api_url(["scr"=>"assets_view","idAsset"=>$asset_fobj->id]),"fa-search",api_text("table-td-view"),null,null,null,null,"_blank");
  $assets_table->addRowField(api_tag("samp",$asset_fobj->barcode),"nowrap");
  $assets_table->addRowField($asset_fobj->getTypology(false,true),"nowrap");
  $assets_table->addRowField($asset_fobj->location,"nowrap");
  $assets_table->addRowField($asset_fobj->name,"truncate-ellipsis");
  //$assets_table->addRowField($ob->render(),"text-right");
 }

 // check for add  action
 if(ACTION=="asset_add"){
  // definitions
  $available_assets_array=array();
  $available_query_where="`fkEmployee` IS NULL AND `status` LIKE 'available'";
  // make query where
  if($_REQUEST['search']){
   $available_query_where.=" AND ( ";
   $available_query_where.="`barcode` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.="OR `name` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.="OR `description` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.="OR `location` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.="OR `note` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.="OR `properties_json` LIKE '%".$_REQUEST['search']."%'";
   $available_query_where.=" )";
  }
  // build query object
  $availables_assets_query=new cQuery("assets__assets",$available_query_where);
  $availables_assets_query->addQueryOrderField("barcode");
  // cycle all results
  foreach($availables_assets_query->getRecords() as $result_f){$available_assets_array[$result_f->id]=new cAssetsAsset($result_f);}
  // build search form
  $search_form="<center><form action='".api_url(["scr"=>"employees_view","act"=>"asset_add","idEmployee"=>$employee_obj->id])."' method='POST' class='form-inline'>";
  $search_form.="<div class='input-group'>";
  $search_form.="<input type='text' name='search' class='form-control' value='".$_REQUEST['search']."' autofocus=autofocus>";
  $search_form.="<span class='input-group-btn'><input type='submit' class='btn btn-primary' value='".api_text("form-fc-search")."'></span>";
  $search_form.="</div></form></center><br>";
  // build table
  $available_assets_table=new strTable(api_text("assets_view-assets-tr-unvalued"));
  $available_assets_table->addHeader("&nbsp;",null,16);
  $available_assets_table->addHeader(api_text("assets_list-th-barcode"),"nowrap");
  $available_assets_table->addHeader(api_text("assets_list-th-typology"),"nowrap");
  $available_assets_table->addHeader(api_text("assets_list-th-location"),"nowrap");
  $available_assets_table->addHeader(api_text("assets_list-th-name"),null,"100%");
  // cycle all assets
  foreach($available_assets_array as $asset_fobj){
   // make table row class
   $tr_class_array=array();
   if($asset_fobj->id==$_REQUEST['idAsset']){$tr_class_array[]="info";}
   if($asset_fobj->deleted){$tr_class_array[]="deleted";}
   // make project row
   $available_assets_table->addRow(implode(" ",$tr_class_array));
   $available_assets_table->addRowFieldAction(api_url(["scr"=>"submit","act"=>"employee_asset_add","idEmployee"=>$employee_obj->id,"idAsset"=>$asset_fobj->id,"return"=>["scr"=>"employees_view","tab"=>"assets"]]),"fa-plus-square",api_text("assets_view-assets-td-add"),api_text("assets_view-assets-td-add-confirm"));
   $available_assets_table->addRowField(api_tag("samp",$asset_fobj->barcode),"nowrap");
   $available_assets_table->addRowField($asset_fobj->getTypology(false,true),"nowrap");
   $available_assets_table->addRowField($asset_fobj->location,"nowrap");
   $available_assets_table->addRowField($asset_fobj->name,"truncate-ellipsis");
  }
  // build modal
  $modal=new strModal(api_text("employees_view-assets-modal-title-add",$employee_obj->name),null,"employees_view-asset");
  $modal->setBody($search_form.$available_assets_table->render());
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_employees_view-asset').modal();});");
 }

?>