<?php
/**
 * Assets
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // definitions
 $module_name="assets";
 $module_repository_url="https://bitbucket.org/SISCAS/cf-assets/";
 $module_repository_version_url="https://bitbucket.org/SISCAS/cf-assets/raw/master/VERSION.txt";
 $module_required_modules=array("employees");
?>