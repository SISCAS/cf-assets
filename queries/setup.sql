--
-- Assets - Setup (1.0.0)
--
-- @package Coordinator\Modules\Assets
-- @company Cogne Acciai Speciali s.p.a
-- @authors Manuel Zavatta <manuel.zavatta@cogne.com>
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `assets__assets`
--

CREATE TABLE IF NOT EXISTS `assets__assets` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `typology` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `barcode` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fkEmployee` int(11) UNSIGNED DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `properties_json` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkEmployee` (`fkEmployee`),
  CONSTRAINT `assets__assets_ibfk_1` FOREIGN KEY (`fkEmployee`) REFERENCES `employees__employees` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `assets__assets__logs`
--

CREATE TABLE IF NOT EXISTS `assets__assets__logs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fkObject` int(11) UNSIGNED NOT NULL,
  `fkUser` int(11) UNSIGNED DEFAULT NULL,
  `timestamp` int(11) UNSIGNED NOT NULL,
  `alert` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `properties_json` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `fkObject` (`fkObject`),
  CONSTRAINT `assets__assets__logs_ibfk_1` FOREIGN KEY (`fkObject`) REFERENCES `assets__assets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('assets-manage','assets',1),
('assets-usage','assets',2);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
