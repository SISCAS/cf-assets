<?php
/**
 * Assets - Template
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // build application
 $app=new strApplication();
 // build nav object
 $nav=new strNav("nav-tabs");
 // dashboard
 $nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
 // assets
 if(explode("_",SCRIPT)[0]=="assets"){
  $nav->addItem(api_text("nav-assets-list"),api_url(["scr"=>"assets_list"]));
  // operations
  if($asset_obj->id && in_array(SCRIPT,array("assets_view","assets_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-assets-operations-edit"),api_url(["scr"=>"assets_edit","idAsset"=>$asset_obj->id]),(api_checkAuthorization("assets-manage")));
  }else{
   $nav->addItem(api_text("nav-assets-add"),api_url(["scr"=>"assets_edit","return"=>["scr"=>"assets_edit"]]),(api_checkAuthorization("assets-manage")));
  }
 }


 // employees
 if(explode("_",SCRIPT)[0]=="employees"){
  $nav->addItem(api_text("nav-employees-list"),api_url(["scr"=>"employees_list"]));
  // operations
  if($employee_obj->id && in_array(SCRIPT,array("employees_view"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-employees-operations-asset-add"),api_url(["scr"=>"employees_view","act"=>"asset_add","tab"=>"assets","idEmployee"=>$employee_obj->id]),(api_checkAuthorization("assets-manage")));
  }
 }


 // add nav to html
 $app->addContent($nav->render(false));
?>