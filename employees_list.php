<?php
/**
 * Assets - Employees List
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("assets-usage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $employees_array=array();
 // set application title
 $app->setTitle(api_text("employees_list"));
 // build filter
 $filter=new strFilter();
 $filter->addSearch(["employees__employees.id","employees__employees.fiscal","employees__employees.firstname","employees__employees.lastname"]);

 $conditions="";
 if(!$conditions){$conditions="1";}
 $conditions.=" AND `assets__assets`.`fkEmployee` IS NOT NULL";

 // build query object
 $query=new cQuery("assets__assets",$filter->getQueryWhere());

 $query->addQueryJoin("employees__employees","id","assets__assets","fkEmployee","inner");


 $query->addQueryOrderField("lastname","asc","employees__employees");
 $query->addQueryOrderField("firstname","asc","employees__employees");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$employees_array[$result_f->id]=new cEmployeesEmployee($result_f);}
 // build table
 $table=new strTable(api_text("employees_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("employees_list-th-id"),"nowrap");
 $table->addHeader(api_text("employees_list-th-fiscal"),"nowrap");
 $table->addHeader(api_text("employees_list-th-fullname"),null,"100%");
 //$table->addHeader("&nbsp;",null,16);
 // cycle all employees
 foreach($employees_array as $employee_fobj){
           // build operation button
           $ob=new strOperationsButton();
           $ob->addElement(api_url(["scr"=>"employees_edit","idEmployee"=>$employee_fobj->id,"return"=>["scr"=>"employees_list"]]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("employees-manage")));
           if($employee_fobj->deleted){$ob->addElement(api_url(["scr"=>"submit","act"=>"employee_undelete","idEmployee"=>$employee_fobj->id,"return"=>["scr"=>"employees_list"]]),"fa-trash-o",api_text("table-td-undelete"),(api_checkAuthorization("employees-manage")),api_text("employees_list-td-undelete-confirm"));}
           else{$ob->addElement(api_url(["scr"=>"submit","act"=>"employee_delete","idEmployee"=>$employee_fobj->id,"return"=>["scr"=>"employees_list"]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("employees-manage")),api_text("employees_list-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($employee_fobj->id==$_REQUEST['idEmployee']){$tr_class_array[]="info";}
  if($employee_fobj->deleted){$tr_class_array[]="deleted";}
  // make employee row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction(api_url(["scr"=>"employees_view","idEmployee"=>$employee_fobj->id]),"fa-search",api_text("table-td-view"));
  $table->addRowField(api_tag("samp",$employee_fobj->id),"nowrap");
  $table->addRowField(api_tag("samp",$employee_fobj->fiscal),"nowrap");
  $table->addRowField($employee_fobj->getLabel(),"truncate-ellipsis");
  //$table->addRowField($ob->render(),"text-right");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");
 api_dump($query->getQuerySQL(),"query sql");
?>