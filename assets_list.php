<?php
/**
 * Assets - Assets List
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("assets-usage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $users_array=array();
 // set application title
 $app->setTitle(api_text("assets_list"));
 // definitions
 $assets_array=array();
 $states_array=array();
 $typologies_array=array();
 $employees_array=array();
 // get states
 foreach(cAssetsAsset::availablesStates() as $status_fobj){$states_array[$status_fobj->code]=$status_fobj->text;}
 // get typologies
 foreach(cAssetsAsset::availablesTypologies() as $typology_fobj){$typologies_array[$typology_fobj->code]=$typology_fobj->text;}
 // get employees
 foreach(cEmployeesEmployee::availables(true) as $employee_fobj){$employees_array[$employee_fobj->id]=$employee_fobj->getLabel(true);}
 // build filter
 $filter=new strFilter();
 $filter->addSearch(["barcode","name","description","properties_json"]);
 $filter->addItem(api_text("assets_list-filter-status"),$states_array,"status");
 $filter->addItem(api_text("assets_list-filter-typology"),$typologies_array,"typology");
 $filter->addItem(api_text("assets_list-filter-employee"),$employees_array,"fkEmployee");
 // build query object
 $query=new cQuery("assets__assets",$filter->getQueryWhere());
 $query->addQueryOrderField("barcode");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$assets_array[$result_f->id]=new cAssetsAsset($result_f);}
 // build table
 $table=new strTable(api_text("assets_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("assets_list-th-barcode"),"nowrap");
 $table->addHeader(api_text("assets_list-th-typology"),"nowrap");
 $table->addHeader(api_text("assets_list-th-location"),"nowrap");
 $table->addHeader(api_text("assets_list-th-employee"),"nowrap");
 $table->addHeader(api_text("assets_list-th-name"),null,"100%");
 $table->addHeader(api_text("assets_list-th-status"),"nowrap text-right");
           //$table->addHeader("&nbsp;",null,16);
 // cycle all assets
 foreach($assets_array as $asset_fobj){
           // build operation button
           /*$ob=new strOperationsButton();
           $ob->addElement(api_url(["scr"=>"assets_edit","idAsset"=>$asset_fobj->id,"return"=>["scr"=>"assets_list"]]),"fa-pencil",api_text("table-td-edit"),(api_checkAuthorization("assets-assets_manage")));
           if($asset_fobj->deleted){$ob->addElement(api_url(["scr"=>"submit","act"=>"asset_undelete","idAsset"=>$asset_fobj->id,"return"=>["scr"=>"assets_list"]]),"fa-trash-o",api_text("table-td-undelete"),(api_checkAuthorization("assets-assets_manage")),api_text("assets_list-td-undelete-confirm"));}
           else{$ob->addElement(api_url(["scr"=>"submit","act"=>"asset_delete","idAsset"=>$asset_fobj->id,"return"=>["scr"=>"assets_list"]]),"fa-trash",api_text("table-td-delete"),(api_checkAuthorization("assets-assets_manage")),api_text("assets_list-td-delete-confirm"));}*/
  // make table row class
  $tr_class_array=array();
  if($asset_fobj->id==$_REQUEST['idAsset']){$tr_class_array[]="info";}
  if($asset_fobj->deleted){$tr_class_array[]="deleted";}
  // make asset row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction(api_url(["scr"=>"assets_view","idAsset"=>$asset_fobj->id]),"fa-search",api_text("table-td-view"));
  $table->addRowField(api_tag("samp",$asset_fobj->barcode),"nowrap");
  $table->addRowField($asset_fobj->getTypology(false,true),"nowrap");
  $table->addRowField($asset_fobj->location,"nowrap");
  $table->addRowField($asset_fobj->getEmployee()->getLabel(),"nowrap");
  $table->addRowField($asset_fobj->name,"truncate-ellipsis");
  $table->addRowField($asset_fobj->getStatus(true,true,"right"),"nowrap text-right");
           //$table->addRowField($ob->render(),"text-right");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");
?>