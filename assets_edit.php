<?php
/**
 * Assets - Assets Edit
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("assets-manage","dashboard");
 // get objects
 $asset_obj=new cAssetsAsset($_REQUEST['idAsset']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($asset_obj->id?api_text("assets_edit",[$asset_obj->getTypology(false,true),$asset_obj->name]):api_text("assets_edit-add")));
 // get form
 $form=$asset_obj->form_edit(["return"=>api_return(["scr"=>"assets_view"])]);
 // additional controls
 if($asset_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"assets_view","idAsset"=>$asset_obj->id]));
  if(!$asset_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"submit","act"=>"asset_delete","idAsset"=>$asset_obj->id]),"btn-danger",api_text("assets_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"submit","act"=>"asset_undelete","idAsset"=>$asset_obj->id,"return"=>["scr"=>"assets_view"]]),"btn-warning");
   $form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"submit","act"=>"asset_remove","idAsset"=>$asset_obj->id]),"btn-danger",api_text("assets_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"assets_list"]));}
 // select script
 $app->addScript("$(document).ready(function(){\$('select[name=\"fkEmployee\"]').select2({allowClear:true,placeholder:\"".api_text("cAssetsAsset-placeholder-fkEmployee")."\"});});");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($asset_obj,"asset");
?>