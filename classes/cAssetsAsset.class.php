<?php
/**
 * Assets - Asset
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

/**
 * Assets Asset class
 */
class cAssetsAsset extends cObject{

  /** Parameters */
  static protected $table="assets__assets";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $status;
  protected $typology;
  protected $barcode;
  protected $name;
  protected $description;
  protected $location;
  protected $fkEmployee;
  protected $note;
  protected $properties_json;

  /**
   * Decode log properties
   *
   * {@inheritdoc}
   */
  public static function log_decode($event,$properties){
   switch($event){
    /** @todo valutare se servono altri eventi */
    case "updated":
     $return_array=array();
     if($properties['status']){$return_array[]=api_text("cAssetsAsset-property-status").": ".cAssetsAsset::availablesStates()[$properties['status']['previous']]->text." &rarr; ".cAssetsAsset::availablesStates()[$properties['status']['current']]->text;}
     if($properties['location']){$return_array[]=api_text("cAssetsAsset-property-location").": ".$properties['location']['previous']." &rarr; ".$properties['location']['current'];}
     if($properties['fkEmployee']){$return_array[]=api_text("cAssetsAsset-property-fkEmployee").": ".(new cEmployeesEmployee($properties['fkEmployee']['previous']))->getLabel()." &rarr; ".(new cEmployeesEmployee($properties['fkEmployee']['current']))->getLabel();}
     $return=implode(" | ",$return_array);
     break;
    /*case "status":
     $return=static::availablesStates()[$properties['previous']]->text." &rarr; ".static::availablesStates()[$properties['current']]->text;
     if($properties['note']){$return.=" - ".$properties['note'];}
     break;*/
   }
   // return
   return $return;
  }

  /**
   * Availables States
   *
   * @return object[] Array of available states
   */
  public static function availablesStates(){
   return array(
    "available"=>(object)array("code"=>"available","text"=>api_text("cAssetsAsset-status-available"),"icon"=>api_icon("fa-archive",api_text("cAssetsAsset-status-available"))),
    "operative"=>(object)array("code"=>"operative","text"=>api_text("cAssetsAsset-status-operative"),"icon"=>api_icon("fa-cog",api_text("cAssetsAsset-status-operative"))),
    "assigned"=>(object)array("code"=>"assigned","text"=>api_text("cAssetsAsset-status-assigned"),"icon"=>api_icon("fa-user",api_text("cAssetsAsset-status-assigned"))),
    "reparation"=>(object)array("code"=>"reparation","text"=>api_text("cAssetsAsset-status-reparation"),"icon"=>api_icon("fa-sliders",api_text("cAssetsAsset-status-reparation"))),
    "discarded"=>(object)array("code"=>"discarded","text"=>api_text("cAssetsAsset-status-discarded"),"icon"=>api_icon("fa-trash",api_text("cAssetsAsset-status-discarded")))
   );
  }

  /**
   * Availables Typologies
   *
   * @return object[] Array of available typologies
   */
  public static function availablesTypologies(){
   return array(
    "sim"=>(object)array(
     "code"=>"sim",
     "text"=>api_text("cAssetsAsset-typology-sim"),
     "icon"=>api_icon("fa-sticky-note-o",api_text("cAssetsAsset-typology-sim")),
     "properties"=>array(
      "serial"=>api_text("cAssetsAsset-property-sim-serial"),
      "number"=>api_text("cAssetsAsset-property-sim-number"),
      "puk"=>api_text("cAssetsAsset-property-sim-puk"),
      "ram"=>api_text("cAssetsAsset-property-sim-ram")
     )
    ),
    "smartphone"=>(object)array(
     "code"=>"smartphone",
     "text"=>api_text("cAssetsAsset-typology-smartphone"),
     "icon"=>api_icon("fa-mobile",api_text("cAssetsAsset-typology-smartphone")),
     "properties"=>array(
      "brand"=>api_text("cAssetsAsset-property-smartphone-brand"),
      "model"=>api_text("cAssetsAsset-property-smartphone-model"),
      "serial"=>api_text("cAssetsAsset-property-smartphone-serial"),
      "imei"=>api_text("cAssetsAsset-property-smartphone-imei")
     )
    ),
    "mobile"=>(object)array(
     "code"=>"mobile",
     "text"=>api_text("cAssetsAsset-typology-mobile"),
     "icon"=>api_icon("fa-id-badge",api_text("cAssetsAsset-typology-mobile")),
     "properties"=>array(
      "brand"=>api_text("cAssetsAsset-property-mobile-brand"),
      "model"=>api_text("cAssetsAsset-property-mobile-model"),
      "serial"=>api_text("cAssetsAsset-property-mobile-serial"),
      "os"=>api_text("cAssetsAsset-property-mobile-os")
     )
    ),
    "notebook"=>(object)array(
     "code"=>"notebook",
     "text"=>api_text("cAssetsAsset-typology-notebook"),
     "icon"=>api_icon("fa-laptop",api_text("cAssetsAsset-typology-notebook")),
     "properties"=>array(
      "brand"=>api_text("cAssetsAsset-property-notebook-brand"),
      "model"=>api_text("cAssetsAsset-property-notebook-model"),
      "serial"=>api_text("cAssetsAsset-property-notebook-serial")
     )
    )
   );
  }

  /**
   * Get Status
   *
   * @param boolean $icon Return icon
   * @param boolean $text Return text
   * @param string $align Icon alignment [left|right]
   * @return string
   */
  public function getStatus($icon=true,$text=true,$align="left"){
   return parent::decode($this->status,static::availablesStates(),$icon,$text,$align);
  }

  /**
   * Get Typology
   *
   * @param boolean $icon Return icon
   * @param boolean $text Return text
   * @param string $align Icon alignment [left|right]
   * @return string
   */
  public function getTypology($icon=true,$text=true,$align="left"){
   return parent::decode($this->typology,static::availablesTypologies(),$icon,$text,$align);
  }

  /**
   * Get Employee
   */
  public function getEmployee(){return new cEmployeesEmployee($this->fkEmployee);}

  /**
   * Get Properties
   */
  public function getProperties(){return json_decode($this->properties_json,true);}

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
   // check properties
   //if(!strlen(trim($this->name))){throw new Exception("Asset name is mandatory..");}
   if(!strlen(trim($this->status))){$this->status="available";}
   if(!array_key_exists($this->status,static::availablesStates())){throw new Exception("Asset status \"".$this->status."\" is not defined..");}
   if(!array_key_exists($this->typology,static::availablesTypologies())){throw new Exception("Asset typology \"".$this->typology."\" is not defined..");}
   // return
   return true;
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"assets","scr"=>"submit","act"=>"asset_store","idAsset"=>$this->id],$additional_parameters)),"POST",null,null,"assets_assets_edit_form");
   // check existence
   if(!$this->exists()){
    $form->addField("select","typology",api_text("cAssetsAsset-property-typology"),$this->typology,api_text("cAssetsAsset-placeholder-typology"),null,null,null,"required");
    foreach(static::availablesTypologies() as $typology_fobj){$form->addFieldOption($typology_fobj->code,$typology_fobj->text);}
   }else{
    $form->addField("static","typology",api_text("cAssetsAsset-property-typology"),$this->getTypology());
   }
   $form->addField("number","barcode",api_text("cAssetsAsset-property-barcode"),$this->barcode,api_text("cAssetsAsset-placeholder-barcode"),null,null,null,"min='100001' max='999999'");
   $form->addField("text","name",api_text("cAssetsAsset-property-name"),$this->name,api_text("cAssetsAsset-placeholder-name"),null,null,null/*,"required"*/);
   $form->addField("textarea","description",api_text("cAssetsAsset-property-description"),$this->description,api_text("cAssetsAsset-placeholder-description"),null,null,null,"rows='2'");
   // check existence
   if(!$this->exists()){
    $form->addField("static",null,api_icon("fa-warning"),api_text("cAssetsAsset-placeholder-properties"));
   }else{
    $form->addField("text","location",api_text("cAssetsAsset-property-location"),$this->location,api_text("cAssetsAsset-placeholder-location"));
    $form->addField("select","fkEmployee",api_text("cAssetsAsset-property-fkEmployee"),$this->fkEmployee,api_text("cAssetsAsset-placeholder-fkEmployee"));
    foreach(cEmployeesEmployee::availables() as $user_fobj){$form->addFieldOption($user_fobj->id,$user_fobj->getLabel(true));}
    $form->addField("textarea","note",api_text("cAssetsAsset-property-note"),$this->note,api_text("cAssetsAsset-placeholder-note"),null,null,null,"rows='2'");
    $form->addField("select","status",api_text("cAssetsAsset-property-status"),$this->status,null,null,null,null,"required");
    foreach(static::availablesStates() as $status_fobj){$form->addFieldOption($status_fobj->code,$status_fobj->text);}
    $form->addField("splitter");
    // cycle all typology properties
    foreach(static::availablesTypologies()[$this->typology]->properties as $property_fkey=>$property_flabel){
     $form->addField("text","properties[".$property_fkey."]",$property_flabel,$this->getProperties()[$property_fkey]);
    }
   }
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  /**
   * Status form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  /*public function form_status(array $additional_parameters=null){
   // build form
   $form=new strForm("?mod=assets&scr=submit&act=asset_status&idAsset=".$this->id."&".http_build_query($additional_parameters),"POST",null,null,"assets_assets_status_form");
   // inputs
   $form->addField("radio","status",api_text("cAssetsAsset-property-status"),$this->status,null,null,null,null,"required");
   foreach(static::availablesStates() as $status_fobj){$form->addFieldOption($status_fobj->code,$status_fobj->text,null,null,null,($status_fobj->code!=$this->status));}
   $form->addField("textarea","note",$GLOBALS['session']->user->fullname."<br>".api_tag("small",api_timestamp_format(time(),api_text("datetime"))),null,api_text("form-input-note-placeholder"),null,null,null,"rows='2'");
   // controls
   $form->addControl("submit",api_text("form-fc-submit"),null,null,null,null,null,false);
   // script button enabler
   $GLOBALS['app']->addScript("$(function(){\$('input[type=radio][name=status]').change(function(){\$('#form_assets_assets_status_form_control_submit').attr('disabled',false)});});");
   // return
   return $form;
  }*/

  // Disable remove function
  public function remove(){throw new Exception("Asset remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

 }

?>