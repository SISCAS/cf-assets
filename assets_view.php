<?php
/**
 * Assets - Assets View
 *
 * @package Coordinator\Modules\Assets
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("assets-usage","dashboard");
 // get objects
 $asset_obj=new cAssetsAsset($_REQUEST['idAsset']);
 // check objects
 if(!$asset_obj->id){api_alerts_add(api_text("cAssetsAsset-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=assets_list");}
 // deleted alert
 if($asset_obj->deleted){api_alerts_add(api_text("cAssetsAsset-warning-deleted"),"warning");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("assets_view",[$asset_obj->getTypology(false,true),$asset_obj->name]));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build asset description list
 $left_dl=new strDescriptionList("br","dl-horizontal");
 $left_dl->addElement(api_text("cAssetsAsset-property-barcode"),api_tag("samp",$asset_obj->barcode));
 $left_dl->addElement(api_text("cAssetsAsset-property-name"),api_tag("strong",$asset_obj->name));
 // build right description list
 $right_dl=new strDescriptionList("br","dl-horizontal");
 if($asset_obj->description){$right_dl->addElement(api_text("cAssetsAsset-property-description"),nl2br($asset_obj->description));}
 if($asset_obj->note){$right_dl->addElement(api_text("cAssetsAsset-property-note"),nl2br($asset_obj->note));}
 // build informations description list
 $informations_dl=new strDescriptionList("br","dl-horizontal");
 $informations_dl->addElement(api_text("cAssetsAsset-property-status"),$asset_obj->getStatus());
 $informations_dl->addElement(api_text("cAssetsAsset-property-typology"),$asset_obj->getTypology());
 if($asset_obj->location){$informations_dl->addElement(api_text("cAssetsAsset-property-location"),$asset_obj->location);}
 if($asset_obj->fkEmployee){$informations_dl->addElement(api_text("cAssetsAsset-property-fkEmployee"),$asset_obj->getEmployee()->getLabel());}
 // build informations description list
 $properties_dl=new strDescriptionList("br","dl-horizontal");
 // cycle all typology properties
 foreach($asset_obj::availablesTypologies()[$asset_obj->typology]->properties as $property_fkey=>$property_flabel){
  $properties_dl->addElement($property_flabel,$asset_obj->getProperties()[$property_fkey]);
 }
 // build view tabs
 $tab=new strTab();
 $tab->addItem(api_icon("fa-flag-o")." ".api_text("assets_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-list")." ".api_text("assets_view-tab-properties"),$properties_dl->render(),("properties"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("assets_view-tab-logs"),api_logs_table($asset_obj->getLogs((!$_REQUEST['all_logs']?10:null)))->render(),("logs"==TAB?"active":null));
 // check for status actions
 if(ACTION=="status" && api_checkAuthorization("assets-manage")){
  // get form
  $form=$asset_obj->form_status(["return"=>["scr"=>"assets_view"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("assets_view-status-modal-title"),null,"assets_view-status");
  $modal->setBody($form->render(2));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_assets_view-status').modal({show:true,backdrop:'static',keyboard:false});});");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($left_dl->render(),"col-xs-12 col-md-5");
 $grid->addCol($right_dl->render(),"col-xs-12 col-md-7");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($asset_obj,"asset");
?>